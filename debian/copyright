Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: plasma-framework
Source: https://invent.kde.org/frameworks/plasma-framework

Files: *
Copyright: 2005-2013, Aaron Seigo <aseigo@kde.org>
           2009, Alan Alpert <alan.alpert@nokia.com>
           2014, Aleix Pol Gonzalez <aleixpol@blue-systems.com>
           2010, Aleix Pol Gonzalez <aleixpol@kde.org>
           2010, Artur Duque de Souza <asouzakde.org>
           2007, Bertjan Broeksema <b.broeksema@kdemail.net>
           2008-2009, Chani Armitage <chani@kde.org>
           2011, Daker Fernandes Pinheiro <dakerfp@gmail.com>
           2014, David Edmundson <davidedmudnson@kde.org>
           2014, David Edmundson <davidedmundson@kde.org>
           2016, David Rosca <nowrep@gmail.com>
           2014, Digia Plc and/or its subsidiary(-ies)
           2015, Eike Hein <hein@kde.org>
           2012, Ivan Cukic <ivan.cukic@kde.org>
           2014, Kai Uwe Broulik <kde@privat.broulik.de>
           2011, Kevin Kofler <kevin.kofler@chello.at>
           2010, Kevin Ottens <ervin@kde.org>
           2010-2016, Marco Martin <mart@kde.org>
           2008-2013, Marco Martin <notmart@gmail.com>
           2012, Marco Martin <notmart@kde.org>
           2011, Mark Gaiser <markg85@gmail.com>
           2010, Martin Blumenstingl <darklight.xdarklight@googlemail.com>
           2013, Martin Gräßlin <mgraesslin@kde.org>
           2007, Matt Broadstone <mbroadst@gmail.com>
           2008, Ménard Alexis <darktears31@gmail.com>
           2010, Ménard Alexis <menard@kde.org>
           2009, Petri Damstén <damu@iki.fi>
           2007, Riccardo Iaconelli <riccardo@kde.org>
           2008, Richard Dale <richard.j.dale@gmail.com>
           2009, Rob Scheepmaker
           2010, Ryan Rix <ry@n.rix.si>
           2011-2015, Sebastian Kügler <sebas@kde.org>
           2010, Siddharth Sharma <siddharth.kde@gmail.com>
           2008, Simon Edwards <simon@simonzone.com>
License: LGPL-2+

Files: debian/*
Copyright: 2014, Scarlett Clark <scarlett@scarlettgatelyclark.com>
License: LGPL-2+

License: BSD-2-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 .
    THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
    IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
    OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
    IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
   THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are
 met:
   * Redistributions of source code must retain the above copyright
     notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright
     notice, this list of conditions and the following disclaimer in
     the documentation and/or other materials provided with the
     distribution.
   * Neither the name of Nokia Corporation and its Subsidiary(-ies) nor
     the names of its contributors may be used to endorse or promote
     products derived from this software without specific prior written
     permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".

License: GPL-2+3+KDEeV
 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License as
 published by the Free Software desktopFoundation; either version 2 of
 the License or (at your option) version 3 or any later version
 accepted by the membership of KDE e.V. (or its successor approved
 by the membership of KDE e.V.), which shall act as a proxy
 defined in Section 14 of version 3 of the license.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 On Debian systems, the complete text of the GNU General Public License
 version 2 can be found in `/usr/share/common-licenses/GPL-2', likewise, the
 complete text of the GNU General Public License version 3 can be found in
 `/usr/share/common-licenses/GPL-3'.

License: LGPL-2
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU Library General Public License version 2 as
 published by the Free Software Foundation
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details
 .
 You should have received a copy of the GNU Library General Public
 License along with this program; if not, write to the
 Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 .
 On Debian systems, the complete text of the GNU Library General Public
 License version 2 can be found in "/usr/share/common-licenses/LGPL-2".

License: LGPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU Library General Public License as
 published by the Free Software Foundation; either version 2, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Library General Public License for more details
 .
 You should have received a copy of the GNU Library General Public
 License along with this program; if not, write to the
 Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA  2.010-1301, USA.
 .
 On Debian systems, the complete text of the GNU Library General Public
 License version 2 can be found in "/usr/share/common-licenses/LGPL-2".

License: LGPL-2.1-QTexception-1.1
 $QT_BEGIN_LICENSE:LGPL$
 No Commercial Usage
 This file contains pre-release code and may not be distributed.
 You may use this file in accordance with the terms and conditions
 contained in the Technology Preview License Agreement accompanying
 this package.
 .
 GNU Lesser General Public License Usage
 Alternatively, this file may be used under the terms of the GNU Lesser
 General Public License version 2.1 as published by the Free Software
 Foundation and appearing in the file LICENSE.LGPL included in the
 packaging of this file.  Please review the following information to
 ensure the GNU Lesser General Public License version 2.1 requirements
 will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
 .
 In addition, as a special exception, Nokia gives you certain additional
 rights.  These rights are described in the Nokia Qt LGPL Exception
 version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
 .
 If you have questions regarding the use of this file, please contact
 Nokia at qt-info@nokia.com.
 .
 $QT_END_LICENSE$
 .
 On Debian systems, the complete text of the GNU Lesser General Public
 License version 2.1 can be found in `/usr/share/common-licenses/LGPL-2.1'.

License: LGPL-2.1+
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 02110-1301  USA
 .
 On Debian systems, the complete text of the GNU Lesser General Public
 License version 2.1 can be found in "/usr/share/common-licenses/LGPL-2.1".
